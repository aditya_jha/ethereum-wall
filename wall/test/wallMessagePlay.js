/*
 * created by aditya on 12/02/18
 */

const WallMessagePlay = artifacts.require("./WallMessagePlay.sol");

contract("WallMessagePlay", function(accounts) {

    it("...should return false", function(done) {
        let wallMessagePlayInstance;
        return WallMessagePlay.deployed().then(function(instance) {
            wallMessagePlayInstance = instance;
            wallMessagePlayInstance.IndexOfValues().watch((error, event) => {
                if(error) {
                    throw error
                }

                console.log('Event:');
                console.log(event);

            });

            wallMessagePlayInstance.allEvents().watch((error, event) => {
                if(error) {
                    throw error
                }

                console.log('Event:');
                console.log(event);

            });
            const sender = web3.eth.coinbase;
            console.log("Sender: ", sender);

            console.log("accounts: ", accounts);
            return wallMessagePlayInstance.addMessage("21.121121", "80.121121", "21.121", "80.121", "Aditya", true, "test", {from: sender, gasLimit: 10000000});
        }).then(is_valid => {
                assert.equal(is_valid, false, "The value 89 was not stored.");
            done();
        }).catch(error => {
            console.log(error);
            done();
        });
    });

});

 