pragma solidity ^0.4.18;


import "./WallMessage.sol";


contract WallOwnership is WallMessage {

    event Transfer(address indexed from, address indexed to, uint256 indexed messageId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed messageId);

    /// @notice Name and symbol of the non fungible token, as defined in ERC721.
    string public name = "AfterWorld";
    string public symbol = "AW";

    function _owns(address _claimant, uint256 _messageId) internal view returns (bool) {
        return messageIndexToOwner[_messageId] == _claimant;
    }

    function _approve(uint256 _messageId, address _approved) internal {
        messageIndexToApproved[_messageId] = _approved;
    }

    function _approvedFor(address _claimant, uint256 _messageId) internal view returns (bool) {
        return messageIndexToApproved[_messageId] == _claimant;
    }

    function implementsERC721() public pure returns (bool) {
        return true;
    }

    function totalSupply() public view returns (uint256 total) {
        return messages.length - 1;
    }

    function balanceOf(address _owner) public view returns (uint256) {
        uint256 count = 0;
        for (uint256 i = 1; i <= totalSupply(); i++) {
            if (messageIndexToOwner[i] == _owner) {
                count++;
            }
        }
        return count;
    }

    function ownerOf(uint256 _messageId) public view returns (address owner) {
        owner = messageIndexToOwner[_messageId];
        require(owner != address(0));
    }

    function approve(address _to, uint256 _messageId) public {
        // Only an owner can grant transfer approval.
        require(_owns(msg.sender, _messageId));

        // Register the approval (replacing any previous approval).
        _approve(_messageId, _to);

        // Emit approval event.
        Approval(msg.sender, _to, _messageId);
    }

    function transferFrom(address _from, address _to, uint256 _messageId) public {
        // Check for approval and valid ownership
        require(_approvedFor(msg.sender, _messageId));
        require(_owns(_from, _messageId));

        // Reassign ownership (also clears pending approvals and emits Transfer event).
        _transfer(_from, _to, _messageId);
    }

    function transfer(address _to, uint256 _messageId) public {
        // Safety check to prevent against an unexpected 0x0 default.
        require(_to != address(0));
        // You can only send your own cat.
        require(_owns(msg.sender, _messageId));

        // Reassign ownership, clear pending approvals, emit Transfer event.
        _transfer(msg.sender, _to, _messageId);
    }

    function tokensOfOwnerByIndex(address _owner, uint256 _index) external view returns (uint256 tokenId) {
        uint256 count = 0;
        for (uint256 i = 1; i <= totalSupply(); i++) {
            if (messageIndexToOwner[i] == _owner) {
                if (count == _index) {
                    return i;
                } else {
                    count++;
                }
            }
        }
        revert();
    }
}