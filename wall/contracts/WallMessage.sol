pragma solidity ^0.4.18;

import "./WallAccessControl.sol";


contract WallMessage is WallAccessControl {

    event NewMessage(address indexed owner, uint256 messageId);

    event Transfer(address indexed from, address indexed to, uint256 indexed messageId);

    struct Message {
        string lat;
        string lng;
        string sender;
        bool messagePublic;
        uint64 timestamp;
        string contentIPFSHash;
    }

    Message[] public messages;

    mapping(uint256 => address) public messageIndexToOwner;

    mapping(bytes32 => uint256) public locationToMessage;

    mapping(uint256 => address) public messageIndexToApproved;

    function changeMessage(uint256 _messageId, string _sender, string _ipfsHash, bool _public) public returns (bool) {
        require(_messageId > 0);
        require(msg.sender == messageIndexToOwner[_messageId]);

        Message memory message = messages[_messageId];
        message.contentIPFSHash = _ipfsHash;
        message.sender = _sender;
        message.messagePublic = _public;

        return true;
    }

    function _transfer(address _from, address _to, uint256 _messageId) internal {
        messageIndexToOwner[_messageId] = _to;
        Transfer(_from, _to, _messageId);
    }

    function _createMessage(
        string _lat,
        string _lng,
        string _sender,
        bool _messagePublic,
        string _contentIPFSHash,
        bytes32 _location,
        address _owner) internal returns (uint) {

        Message memory message = Message({
            lat : _lat,
            lng : _lng,
            sender : _sender,
            messagePublic : _messagePublic,
            timestamp : uint64(now),
            contentIPFSHash : _contentIPFSHash
        });

        uint256 messageId = messages.push(message) - 1;

        locationToMessage[_location] = messageId;

        NewMessage(_owner, messageId);

        _transfer(0, _owner, messageId);

        return messageId;
    }
}
