pragma solidity ^0.4.18;


import "./WallOwnership.sol";
import "./Strings.sol";


contract WallMessagePlay is WallOwnership {

    using Strings for string;

    event IndexOfValues(string indexed key, uint256 indexed value);

    address public newContractAddress;

    function WallMessagePlay() public {
        chairman = msg.sender;
        director = msg.sender;

        _createMessage(
                "-56.690240",
                "100.242762",
                "Aditya",
                false,
                "QmRpvcoARBK9q8KLwjZLQf1f69686ycZNNfEZVjXCNrd4F",
                0x2D35362E3639303130302E323432000000000000000000000000000000000000,
                msg.sender
        );
    }

    function setNewAddress(address _newAddress) public onlyBoard whenHalted {
        newContractAddress = _newAddress;
        ContractUpgrade(_newAddress);
    }

    function _stringToBytes32(string s) internal pure returns (bytes32 result) {
        if (bytes(s).length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(s, 32))
        }
    }

    function _concatStr(string a, string b) internal pure returns (bytes32 result) {
        bytes memory a1 = bytes(a);
        bytes memory b1 = bytes(b);

        string memory _tmpValue = new string(a1.length + b1.length);
        bytes memory _retValue = bytes(_tmpValue);

        uint i;
        uint j;

        for (i = 0; i < a1.length; i++) {
            _retValue[j++] = a1[i];
        }

        for (i = 0; i < b1.length; i++) {
            _retValue[j++] = b1[i];
        }

        assembly {
            result := mload(add(_retValue, 32))
        }
    }

    function _isValidLocation(
        string _lat,
        string _lng,
        string _latStripped,
        string _lngStripped
    ) public pure returns (bool) {

        int256  _latDecimal = _lat.indexOf(".");
        int256 _lngDecimal = _lng.indexOf(".");

        int256 _latStrippedDecimal = _latStripped.indexOf(".");
        int256 _lngStrippedDecimal = _lngStripped.indexOf(".");

        require(int(bytes(_lat).length) - _latDecimal == 7);
        require(int(bytes(_lng).length) - _lngDecimal == 7);
        require(int(bytes(_latStripped).length) - _latStrippedDecimal == 4);
        require(int(bytes(_lngStripped).length) - _lngStrippedDecimal == 4);

        string memory sub = _lat.substring(_latDecimal + 4);
        string memory sub2 = _lng.substring(_latDecimal + 4);
        require(sub.compareTo(_latStripped));
        require(sub2.compareTo(_lngStripped));

        return true;
    }

    function addMessage(
        string _lat,
        string _lng,
        string _latStripped,
        string _lngStripped,
        string _sender,
        bool _messagePublic,
        string _contentIPFSHash
    ) public whenNotHalted returns (uint256) {
        // if you sent a location not on google maps
        // you lost it

        // check if input is valid
        require(_isValidLocation(_lat, _lng, _latStripped, _lngStripped));

        bytes32 location = _concatStr(_latStripped, _lngStripped);

        require(locationToMessage[location] == 0);

        return _createMessage(_lat, _lng, _sender, _messagePublic, _contentIPFSHash, location, msg.sender);
    }
}

