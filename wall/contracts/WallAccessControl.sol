pragma solidity ^0.4.18;


contract WallAccessControl {
    /// This event is emitted when the contract is upgraded
    event ContractUpgrade(address newContract);

    // The Chairman and Managing Director of the Contract
    address public chairman;
    address public director;

    // Switch to halt the contract execution
    bool public halted = false;

    /// Access modifier for chairman only functionality
    modifier onlyChairman() {
        require(msg.sender == chairman);
        _;
    }

    /// Access modifier for chairman only functionality
    modifier onlyDirector() {
        require(msg.sender == director);
        _;
    }

    /// Access modifier for board members only functionality
    modifier onlyBoard() {
        require(msg.sender == chairman || msg.sender == director);
        _;
    }

    function withdrawBalance() external onlyBoard {
        chairman.transfer(this.balance);
    }

    /// Set a new chairman of the board
    function setChairman(address newChairman) public onlyChairman {
        require(newChairman != address(0));

        chairman = newChairman;
    }

    /// Set a new director of the board
    function setDirector(address newDirector) public onlyChairman {
        require(newDirector != address(0));

        director = newDirector;
    }

    /// modifier to control contract functionality via halting
    /// Control modifier to allow execution only when contract IS NOT halted
    modifier whenNotHalted() {
        require(!halted);
        _;
    }

    modifier whenHalted() {
        require(halted);
        _;
    }

    /// Halt the execution of the contract
    function halt() public onlyBoard whenNotHalted {
        halted = true;
    }

    /// Resume the execution of the contract
    function start() public onlyBoard whenHalted {
        halted = false;
    }
}
