/*
 * created by aditya on 05/02/18
 */


export function getFormattedLatLng(lat, lng) {
    function _getFormattedLatLngHelper(item) {
        const decimal = item.indexOf(".");
        return {
            _item: item.substring(0, decimal + 7),
            _itemStripped: item.substring(0, decimal + 4)
        }
    }

    const _lat = _getFormattedLatLngHelper(lat.toString());
    const _lng = _getFormattedLatLngHelper(lng.toString());

    return {
        _lat: _lat._item,
        _lng: _lng._item,
        _latStripped: _lat._itemStripped,
        _lngStripped: _lng._itemStripped
    }
}
