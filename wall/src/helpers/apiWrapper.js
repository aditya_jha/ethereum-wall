/*
 * created by aditya on 30/01/18
 */

import axios from "axios";

export function makeRequest(config) {
    return axios.request({
        method: config.method || "get",
        baseURL: config.baseURL || window.API_BASE,
        url: config.url,
        data: config.data,
        headers: config.headers || {},
        params: config.params,
        timeout: config.timeout || 100000,
        cancelToken: config.cancelToken
    }).then(result => {
        return result.data
    });
}