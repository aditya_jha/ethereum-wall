/*
 * created by aditya on 29/01/18
 */

export const HOME = "/";
export const POST_NEW_MESSAGE = "/post-new-message";
export const BROWSE_MESSAGES = "/browse-messages";
export const BROWSE_MESSAGES_LIST = "/browse-messages-list";
export const MY_MESSAGES = "/browse-messages/my-messages";
export const ABOUT_US = "/about-us";
export const ERROR_PAGE = "/error?return=";
export const POST_NEW_MESSAGE_PREVIEW = "/post-new-message/preview";