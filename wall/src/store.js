/*
 * created by aditya on 29/01/18
 */

import {
    createStore,
    applyMiddleware,
    compose
} from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import reducers from "./reducers";

let middleware = [thunk, logger];

export default function configureStore() {
    // const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const composeEnhancers = compose;
    return createStore(
        reducers,
        composeEnhancers(applyMiddleware(...middleware))
    )
}
