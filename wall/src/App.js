import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Provider} from "react-redux";
import 'typeface-roboto';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Web3 from "web3";

import Header from "./components/Header";

import Routes from "./routes";
import configureStore from "./store";
import MuiThemeConfig from "./materialUIThemeConfig";
import {WEB3JS_NETWORK_ID, WEB3JS_SET_REFERENCE, SET_CONTRACT_REFERENCE} from "./reducers/web3js";
import MyWeb3 from "./models/MyWeb3";
// import * as URL from "./data/Urls";

import WallMessagePlayContract from "./../build/contracts/WallMessagePlay";

import './App.css'

import "./../node_modules/react-quill/dist/quill.snow.css";
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';

const store = configureStore();

window.API_BASE = "http://127.0.0.1:8082";

const muiTheme = getMuiTheme(MuiThemeConfig);

//import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
//const muiTheme = getMuiTheme(darkBaseTheme);

class App extends Component {
    constructor(props) {
        super();
    }

    componentDidMount() {
        if (typeof window.web3 !== "undefined") {
            // Use Mist/MetaMask's provider
            const web3js = new Web3(window.web3.currentProvider);
            const myWeb3Instance = new MyWeb3();
            myWeb3Instance.setWeb3Instance(web3js);

            store.dispatch({
                type: WEB3JS_SET_REFERENCE,
                web3js: myWeb3Instance
            });

            const {abi, networks} = WallMessagePlayContract;
            myWeb3Instance.getEthereumNetworkVersion().then(netId => {
                const {address} = networks[netId];
                store.dispatch({
                    type: SET_CONTRACT_REFERENCE,
                    contract: myWeb3Instance.getContractReference(address, abi)
                });

                store.dispatch({
                    network: netId,
                    type: WEB3JS_NETWORK_ID
                });
            }).catch(error => {
                console.log(error);
                // this.props.history.push(URL.ERROR_PAGE + this.props.location.pathname);
            });
        } else {
            console.log('No web3? You should consider trying MetaMask!')
            // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
            /* TODO: leaving this for now */
        }
    }

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <MuiThemeProvider muiTheme={muiTheme}>
                        <div>
                            <Header/>
                            <Routes/>
                        </div>
                    </MuiThemeProvider>
                </Router>
            </Provider>

        );
    }
}

export default App
