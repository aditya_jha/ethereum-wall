/*
 * created by aditya on 29/01/18
 */

import React from "react";
import {Route, Switch} from "react-router-dom";
import * as URL from "./../data/Urls";
import {
    Home,
    _404,
    PostMessage,
    AboutUs,
    BrowseMessages,
    MyMessages,
    PreviewMessage
} from "../components";

const Routes = () => {
    return (
        <Switch>
            <Route exact path={URL.HOME} component={Home}/>
            <Route exact path={URL.POST_NEW_MESSAGE} component={PostMessage}/>
            <Route path={URL.ABOUT_US} component={AboutUs}/>
            <Route exact path={URL.BROWSE_MESSAGES} component={BrowseMessages}/>
            <Route exact path={URL.MY_MESSAGES} component={MyMessages}/>
            <Route exact path={URL.POST_NEW_MESSAGE_PREVIEW} component={PreviewMessage}/>
            <Route path="*" component={_404}/>
        </Switch>
    )
};

export default Routes;