/*
 * created by aditya on 06/02/18
 */

export const NEW_MESSAGE_SET_CONTENT = "NEW_MESSAGE_SET_CONTENT";
export const NEW_MESSAGE_SET_SENDER = "NEW_MESSAGE_SET_SENDER";
export const NEW_MESSAGE_SET_PUBLIC_VISIBILITY = "NEW_MESSAGE_SET_PUBLIC_VISIBILITY";
export const NEW_MESSAGE_SET_ANONYMITY = "NEW_MESSAGE_SET_ANONYMITY";
export const NEW_MESSAGE_SET_CONTENT_IPFS_HASH = "NEW_MESSAGE_SET_CONTENT_IPFS_HASH";
export const NEW_MESSAGE_SET_THEME = "NEW_MESSAGE_SET_THEME";
export const NEW_MESSAGE_SET_LOCATION = "NEW_MESSAGE_SET_LOCATION";

const defaultState = {
    id: new Date().valueOf(),
    selectedTheme: {id: 1},
    selectedLocation: {
        lat: 19.975404849273303, lng: 78.14991171874999
    },
    message: "Who cares?",
    anonymous: false,
    sender: "Aditya Jha",
    messagePublic: true,
    messageIPFSHash: "QmRpvcoARBK9q8KLwjZLQf1f69686ycZNNfEZVjXCNrd4F"
};

export function newMessage(state = defaultState, action) {
    switch (action.type) {
        case NEW_MESSAGE_SET_ANONYMITY:
            return {
                ...state,
                anonymous: action.anonymous
            };
        case NEW_MESSAGE_SET_CONTENT:
            return {
                ...state,
                message: action.message
            };
        case NEW_MESSAGE_SET_SENDER:
            return {
                ...state,
                sender: action.sender
            };
        case NEW_MESSAGE_SET_PUBLIC_VISIBILITY:
            return {
                ...state,
                messagePublic: action.messagePublic
            };
        case NEW_MESSAGE_SET_CONTENT_IPFS_HASH:
            return {
                ...state,
                messageIPFSHash: action.messageIPFSHash
            };
        case NEW_MESSAGE_SET_THEME:
            return {
                ...state,
                selectedTheme: action.theme
            };
        case NEW_MESSAGE_SET_LOCATION:
            return {
                ...state,
                selectedLocation: action.selectedLocation
            };
        default:
            return state;
    }
}