/*
 * created by aditya on 14/02/18
 */

export const MESSAGES_IS_LOADING = "MESSAGES_IS_LOADING";
export const MESSAGES_INIT_MESSAGES = "MESSAGES_INIT_MESSAGES";
export const MESSAGES_CLEAR_MESSAGES = "MESSAGES_CLEAR_MESSAGES";
export const MESSAGES_ERROR_LOADING = "MESSAGES_ERROR_LOADING";

const defaultState = {
    isLoading: false,
    messages: [],
    error: false
};

export function messages(state = defaultState, action) {
    switch (action.type) {
        case MESSAGES_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case MESSAGES_ERROR_LOADING:
            return {
                ...state,
                error: action.error
            };
        case MESSAGES_CLEAR_MESSAGES:
            return {
                ...state,
                messages: []
            };
        case MESSAGES_INIT_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };
        default:
            return state;
    }
}
 