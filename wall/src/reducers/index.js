/*
 * created by aditya on 29/01/18
 */

import {combineReducers} from "redux";
import {web3js} from "./web3js";
import {newMessage} from "./newMessage";
import {messages} from "./messages";

export default combineReducers({
    web3js,
    newMessage,
    messages
});
