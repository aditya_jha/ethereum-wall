/*
 * created by aditya on 14/02/18
 */

import React from "react";


const Marker = ({icon}) => {
    return (
        <div>
            <img src={icon} role="presentation" style={{width: "2em", height: "2em"}} />
        </div>
    );
};

export default Marker;
