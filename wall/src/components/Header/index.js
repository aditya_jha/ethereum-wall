/*
 * created by aditya on 03/02/18
 */

import React from "react";
import AppBar from "material-ui/AppBar";
import * as URL from "../../data/Urls";
import {Link, withRouter} from "react-router-dom";
import {APP_NAME} from "./../../data/variables";

const Header = withRouter(({history}) => {
    const linkLayoutStyle = {
        margin: "5px 10px"
    };

    const links = [{
        name: "Home",
        to: URL.HOME
    }, {
        name: "Post Message",
        to: URL.POST_NEW_MESSAGE
    }, {
        name: "Browse",
        to: URL.BROWSE_MESSAGES
    }, {
        name: "My Messages",
        to: URL.MY_MESSAGES
    }, {
        name: "About Us",
        to: URL.ABOUT_US
    }];

    return (
        <AppBar
            style={{position: 'fixed'}}
            onLeftIconButtonClick={() =>  history.push(URL.HOME)}
            onTitleClick={() =>  history.push(URL.HOME)}
            title={APP_NAME}
            titleStyle={{cursor: "pointer", display: "inline"}}>
            <div style={{marginTop: 21}}>
                {links.map((link, index) => (
                    <span style={link.style || linkLayoutStyle} key={index}>
                        <Link to={link.to}>{link.name}</Link>
                    </span>
                ))}
            </div>
        </AppBar>
    );
});

export default Header;