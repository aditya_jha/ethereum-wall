/*
 * created by aditya on 29/01/18
 */

import React from "react";
import Footer from "./../Footer";

import {APP_NAME} from "./../../data/variables";

const Home = () => {
    return (
        <div>
            <div className="home_page_content_container" style={{height: "80vh"}}>
                <div className="vertical-middle table-cell">
                    <video
                        style={{width: "100vw"}}
                        autoPlay
                        muted
                        loop
                        poster={'https://www.w3schools.com/w3images/lights.jpg'}
                        src={'http://127.0.0.1:8082/videos/homevideo.mp4'}>
                    </video>
                </div>
            </div>
            <div className="container">
                <div className="home_page_content_container">
                    <div className="vertical-middle table-cell">
                        <div className="home_page_content_wrapper">
                            <h4>{APP_NAME} is one of the first platform built on blockchain technology to leave your virtual mark on the world. With this, your presence on the blockchain world is guranteed to persist for generations to come.</h4>
                        </div>
                    </div>
                </div>
                <div className="home_page_content_container">
                    <div className="vertical-middle table-cell">
                        <div className="home_page_content_wrapper" style={{float: "right"}}>
                            <h4>{APP_NAME} is a decentralised and connected world powered by Ethereum technology where users can drop notes for their loved ones and the world to see</h4>
                        </div>
                    </div>
                </div>
                <div className="home_page_content_container">
                    <div className="vertical-middle table-cell">
                        <div className="home_page_content_wrapper">
                            <h4>{APP_NAME} runs on the same blockchain technology as Ethereum. Just like each individual coin, each message is uniquely linked to Ethereum token based on ERC-721 proposal. To post a message select your theme, write  message and post it anywhere on the world by sending Ether to the contract using Metamask.</h4>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
};

export default Home;
