/*
 * created by aditya on 03/02/18
 */

import React from "react";
import * as URL from "../../data/Urls";
import {Link, withRouter} from "react-router-dom";

const Footer = withRouter(({history}) => {
    const linkLayoutStyle = {
        margin: "5px 10px"
    };

    const links = [{
        name: "Home",
        to: URL.HOME
    }, {
        name: "Post Message",
        to: URL.POST_NEW_MESSAGE
    }, {
        name: "Browse",
        to: URL.BROWSE_MESSAGES
    }, {
        name: "My Messages",
        to: URL.MY_MESSAGES
    }, {
        name: "About Us",
        to: URL.ABOUT_US
    }];


    return (
        <section id="footer">
            <div className="container">
                    {/*<div className="row">*/}
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <h3>Products</h3>
                            <ul>
                                <li><a href="http://wingthemes.com/">WingThemes</a>
                                </li>
                                <li><a href="http://graygrids.com/">Graygrids</a>
                                </li>
                                <li><a href="http://wpbean.com/">WPBean</a>
                                </li>
                                <li><a href="http://landingbow.com/">Landingbow</a>
                                </li>
                                <li><a href="http://freebiescircle.com/">FreebiesCicle</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <h3>FAQs</h3>
                            <ul>
                                <li><a href="#">Why choose us?</a>
                                </li>
                                <li><a href="#">Where we are?</a>
                                </li>
                                <li><a href="#">Fees</a>
                                </li>
                                <li><a href="#">Guarantee</a>
                                </li>
                                <li><a href="#">Discount</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <h3>About</h3>
                            <ul>
                                <li><a href="#">Career</a>
                                </li>
                                <li><a href="#">Partners</a>
                                </li>
                                <li><a href="#">Team</a>
                                </li>
                                <li><a href="#">Clients</a>
                                </li>
                                <li><a href="#">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <h3>Find us on</h3>
                            <a className="social" href="#" target="_blank"><i className="fa fa-facebook"></i></a>
                            <a className="social" href="#" target="_blank"><i className="fa fa-twitter"></i></a>
                            <a className="social" href="#" target="_blank"><i className="fa fa-google-plus"></i></a>
                        </div>
                    {/*</div>*/}
            </div>
        </section>
    );
});

export default Footer;