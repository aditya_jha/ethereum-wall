/*
 * created by aditya on 03/02/18
 */

import React from "react";
import {getMessages} from "../BrowseMessages/actions";
import DisplayMessagesMap from "./../DisplayMessagesMap";

export default class MyMessages extends React.Component {
    constructor(props) {
        super();
        this.state = {
            messages: []
        };
    }

    componentWillMount() {
        // get address from meta mask
        getMessages(true).then(result => {
            this.setState({
                messages: result
            });
        })
    }

    render() {
        const {messages} = this.state;

        return (
            <DisplayMessagesMap messages={messages} history={this.props.history}/>
        )
    }
}