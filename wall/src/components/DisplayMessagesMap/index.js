/*
 * created by aditya on 04/02/18
 */

import React from "react";
import Map from "./../Map";
import {Card, CardText, CardHeader, CardActions} from "material-ui/Card";
import Close from "material-ui/svg-icons/navigation/close";
import AccessTime from "material-ui/svg-icons/device/access-time";
import FlatButton from "material-ui/FlatButton";
import SocialShare from "material-ui/svg-icons/social/share";
import ArrowDropdownCircle from "material-ui/svg-icons/navigation/arrow-drop-down-circle";
import {white} from "material-ui/styles/colors";
import * as URL from "./../../data/Urls";
import Marker from "./../Marker";

export default class DisplayMessagesMap extends React.Component {
    constructor(props) {
        super();
        this.state = {
            clickedMessage: props.messages.length === 1 ? props.messages[0] : {},
        };
    }

    componentWillUnmount() {
        this.setState({
            clickedMessage: {}
        });
    }

    render() {
        const {clickedMessage} = this.state;
        const {messages, preview} = this.props;

        let mapChildren = messages.map((message, index) => (
            <Marker index={index} key={message.id} lat={message.lat} lng={message.lng} icon={message.theme.icon_url}/>
        ));

        const closeIconElement = (
            <Close onClick={this.closeMessageCard.bind(this)}/>
        );

        return (
            <div>
                <div style={{height: "91vh"}}>
                    <Map
                        zoom={1}
                        center={{lat: clickedMessage.lat, lng: clickedMessage.lng}}
                        childComponents={mapChildren}
                        onChildClick={this.onMapMarkerClicked.bind(this)}
                    />

                    {!preview && <div style={{position: "absolute", top: 70, left: 10}}>
                        <input type="text"
                               placeholder="search location"
                               name="location_search"
                               style={{height: 25, width: 200, fontSize: 14}}/>
                    </div>}

                    {clickedMessage && clickedMessage.theme &&
                    <Card style={{position: "absolute", top: 70, right: 20, maxHeight: 500, width: 300}}>
                        <CardHeader
                            openIcon={closeIconElement}
                            closeIcon={closeIconElement}
                            showExpandableButton={true}
                            title={clickedMessage.sender}
                            subtitle="says"
                            avatar={clickedMessage.theme.icon}>
                        </CardHeader>
                        <CardText>
                            <div>
                                <div
                                    dangerouslySetInnerHTML={{__html: clickedMessage.content}}
                                    style={{overflow: "scroll", maxHeight: 300}}
                                />
                                <div style={{marginTop: "1em"}}>
                                    <AccessTime/>
                                    <span style={{
                                        margin: "5px 0 0 10px",
                                        position: "absolute",
                                        fontSize: "small",
                                        color: "grey"
                                    }}>{new Date(clickedMessage.timestamp).toLocaleString()}</span>
                                </div>
                            </div>
                        </CardText>
                        <CardActions>
                            <FlatButton icon={<SocialShare/>} fullWidth={true}/>
                        </CardActions>
                    </Card>
                    }
                    {messages.length && !preview &&
                    <div style={{position: "absolute", bottom: 0, height: 30, width: "100%", textAlign: "center"}}>
                        <ArrowDropdownCircle color={white} style={{width: 30, height: 30, cursor: "pointer"}}
                                             onClick={this.listViewButtonClicked}/>
                    </div>
                    }
                </div>
            </div>
        );
    }

    onMapMarkerClicked = (key, childProps) => {
        this.setState({
            clickedMessage: this.props.messages[childProps.index]
        });
    };

    closeMessageCard = () => {
        this.setState({
            clickedMessage: {}
        });
    };

    listViewButtonClicked = () => {
        this.props.history.push(URL.BROWSE_MESSAGES_LIST);
    };
}

