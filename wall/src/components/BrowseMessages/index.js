/*
 * created by aditya on 03/02/18
 */

import React from "react";
import DisplayMessagesMap from "./../DisplayMessagesMap";

import {getMessages} from "./actions";

export default class BrowseMessages extends React.Component {
    constructor(props) {
        super();
        this.state = {
            messages: []
        };
    }

    componentWillMount() {
        getMessages().then(result => {
            this.setState({
                messages: result
            });
        })
    }

    render() {
        const {messages} = this.state;

        return (
            <DisplayMessagesMap messages={messages} history={this.props.history}/>
        )
    }
}