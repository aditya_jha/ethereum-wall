/*
 * created by aditya on 04/02/18
 */

import {makeRequest as fetch} from "../../helpers/apiWrapper";
import {
    MESSAGES_IS_LOADING,
    MESSAGES_INIT_MESSAGES
} from "./../../reducers/messages";

export function getMessages(address) {
    let url = "/messages";
    if (address) {
        url = `/messages?address=${address}`;
    }
    return new Promise((resolve, reject) => {
        fetch({
            url: url
        }).then(result => {
            resolve(result.data);
        }).catch(error => {
            reject(error);
        })
    })
}

export function messageIsLoading(isLoading) {
    return {
        type: MESSAGES_IS_LOADING,
        isLoading
    }
}

export function initMessages(messages) {
    return {
        type: MESSAGES_INIT_MESSAGES,
        messages
    }
}