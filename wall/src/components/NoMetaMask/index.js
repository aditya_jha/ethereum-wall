/*
 * created by aditya on 05/02/18
 */

import React from "react";
import RaisedButton from "material-ui/RaisedButton";

export default class NoMetaMask extends React.Component {
    constructor(props) {
        super();
        this.state = {
            installClicked: false
        };
    }

    render() {
        const {installClicked} = this.state;

        return (
            <div>
                {!installClicked &&
                    <div>
                        <h2>
                            You’ll need a safe place to store all of your messages! The perfect place is in a secure wallet like MetaMask. This will also act as your login to the game (no extra password needed).
                        </h2>
                        <div style={{margin: "1em 0", textAlign: "center"}}>
                            <RaisedButton label="Install MetaMask" secondary={true} onClick={this.installMetaMaskButton}/>
                        </div>
                    </div>
                }
                {installClicked &&
                    <div>
                        <h1>Finish installing MetaMask to continue</h1>
                        <br/>
                        <h3>Make sure you follow the instructions on MetaMask to finish the installation.</h3>
                        <div style={{margin: "1em 0", textAlign: "center"}}>
                            <RaisedButton label="I Installed MetaMask" secondary={true} onClick={this.installedNotifyButton.bind(this)}/>
                        </div>
                    </div>
                }
            </div>
        )
    }

    installMetaMaskButton = () => {
        this.setState({
            installClicked: true
        });
        window.open("https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en", "_blank");
    };

    installedNotifyButton = () => {
        window.location.reload();
    }
}