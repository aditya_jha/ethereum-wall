/*
 * created by aditya on 30/01/18
 */

import React from "react";
import {connect} from "react-redux";
import {GridList, GridTile} from "material-ui/GridList";
import Paper from 'material-ui/Paper';
import {
    Step,
    Stepper,
    StepButton,
    StepContent,
} from 'material-ui/Stepper';
import Snackbar from 'material-ui/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from "material-ui/TextField";
import Toggle from 'material-ui/Toggle';
import Map from "./../Map";
import NoMetaMask from "./../NoMetaMask";
import * as URL from "./../../data/Urls";
import {
    NEW_MESSAGE_SET_CONTENT,
    NEW_MESSAGE_SET_SENDER,
    NEW_MESSAGE_SET_PUBLIC_VISIBILITY,
    NEW_MESSAGE_SET_ANONYMITY,
    NEW_MESSAGE_SET_CONTENT_IPFS_HASH,
    NEW_MESSAGE_SET_THEME,
    NEW_MESSAGE_SET_LOCATION
} from "./../../reducers/newMessage";
import {
    messageIsLoading,
    getMessages,
    initMessages
} from "./../BrowseMessages/actions";

import {getThemes} from "./actions";
import Editor from "./../EditorComponent";
import Marker from "./../Marker";

class PostMessage extends React.Component {
    static defaultProps = {
        center: {lat: 20.5937, lng: 78.9629},
        zoom: 5
    };

    constructor(props) {
        super();
        this.state = {
            themes: [],
            stepIndex: 0,
            openSnackbar: false,
            snackbarMessage: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.myWeb3 && nextProps.contract) {
            try {
                nextProps.myWeb3.getBalance(nextProps.myWeb3.getAccountOwner(), true)
                    .then(balance => {
                        console.log("Ether balance in main account is: ", balance);
                    });
                nextProps.contract.get((err, result) => {
                    console.log(result.toNumber());
                });

            } catch (e) {
                console.log(e);
            }
        }
    }

    componentWillMount() {
        getThemes().then(data => {
            this.setState({
                themes: data
            })
        });
        this.props.fetchAllMessages();
    }

    render() {
        const {
            title,
            myWeb3,
            messages,
            handleMessageContentChange,
            setClickedTheme,
            handleMessagePublicSwitchToggle,
            handleNameChange,
            handleAnonymousSwitchToggle
        } = this.props;
        const {stepIndex, themes, openSnackbar} = this.state;
        const {selectedTheme, message, selectedLocation, anonymous, messagePublic} = this.props.newMessage;

        const styles = {
            root: {
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-around',
                padding: 25
            },
            gridList: {
                margin: "0 auto",
                // height: 450,
                overflowY: 'auto',
                display: 'flex',
                flexWrap: 'wrap',
                overflowX: 'auto',
            },
            gridTile: {
                zDepth: 1,
            }
        };

        if (!myWeb3) {
            return (
                <div style={{margin: "0 auto", width: 600}}>
                    <div style={{padding: "1em"}}>
                        <NoMetaMask {...this.props} />
                    </div>
                </div>
            )
        }

        let mapChildren = messages.messages.map(message => (
            <Marker key={message.id} lat={message.lat} lng={message.lng} icon={message.theme.icon_url}/>
        ));
        const key = new Date().valueOf();
        if (selectedLocation.lat && selectedLocation.lng && selectedTheme.icon_url) {
            mapChildren.push(<Marker key={key} lat={selectedLocation.lat} lng={selectedLocation.lng} icon={selectedTheme.icon_url}/>)
        }

        return (
            <div>
                <h2>{title}</h2>
                <div style={{maxWidth: 800, margin: 'auto'}}>
                    <Stepper
                        activeStep={stepIndex}
                        linear={false}
                        orientation="vertical">
                        <Step>
                            <StepButton onClick={this.changeStepperIndex.bind(this, 0)}>
                                Select Theme
                            </StepButton>
                            <StepContent>
                                <div style={styles.root}>
                                    <GridList
                                        cols={4}
                                        cellHeight={"auto"}
                                        children={themes.length}
                                        padding={20}>

                                        {themes.map(tile => (
                                            <Paper
                                                key={tile.id}
                                                rounded={true}
                                                zDepth={selectedTheme.name === tile.name ? 5 : 2}>
                                                <GridTile
                                                    onClick={setClickedTheme.bind(this, tile)}
                                                    rows={1}
                                                    cols={1}>
                                                    <div style={{
                                                        padding: 15,
                                                        background: selectedTheme.name === tile.name ? "rgba(0,188,212,0.5)" : ""
                                                    }}>
                                                        <img src={tile.icon_url} alt={tile.name}
                                                             width={"100%"}/>
                                                        <p style={{
                                                            paddingTop: 10,
                                                            textAlign: "center",
                                                            textTransform: "capitalize"
                                                        }}>{tile.name}</p>
                                                    </div>

                                                </GridTile>
                                            </Paper>
                                        ))}
                                    </GridList>
                                </div>
                                {this.renderStepActions(0)}
                            </StepContent>
                        </Step>
                        <Step>
                            <StepButton onClick={this.changeStepperIndex.bind(this, 1)}>
                                Place your marker
                            </StepButton>
                            <StepContent>
                                <div>
                                    <div style={{height: 350, width: '100%'}}>
                                        <Map onClick={this.onMapClick.bind(this)} childComponents={mapChildren}/>
                                    </div>
                                    <div style={{position: "absolute", top: "5px", left: "5px"}}>
                                        <input type="text"
                                               placeholder="search location"
                                               name="location_search"
                                               style={{height: 25, width: 200, fontSize: 14}}/>
                                    </div>
                                </div>
                                {this.renderStepActions(1)}
                            </StepContent>
                        </Step>
                        <Step>
                            <StepButton onClick={this.changeStepperIndex.bind(this, 2)}>
                                Message
                            </StepButton>
                            <StepContent>
                                <div>
                                    <TextField
                                        disabled={anonymous}
                                        onChange={handleNameChange}
                                        hintText="Aditya Jha"
                                        floatingLabelText="Who is posting the message?"/>
                                    <br/><br/>
                                    <Toggle
                                        labelPosition="right"
                                        toggled={anonymous}
                                        onToggle={handleAnonymousSwitchToggle}
                                        label="Stay anonymous instead"/>
                                    <br/><br/>
                                    <Editor
                                        content={message}
                                        placeHolder="Enter your message here"
                                        onChange={handleMessageContentChange.bind(this)}
                                    />
                                    <br/><br/>
                                    <Toggle
                                        labelPosition="right"
                                        toggled={messagePublic}
                                        onToggle={handleMessagePublicSwitchToggle}
                                        label="Is Message Publicly Available?"/>
                                </div>
                                {this.renderStepActions(2)}
                            </StepContent>
                        </Step>
                    </Stepper>
                </div>
                <Snackbar
                    open={openSnackbar}
                    message="Please fill proper values"
                    autoHideDuration={2000}
                />
            </div>
        )
    }

    handleNext = () => {
        const {stepIndex} = this.state;
        if (stepIndex < 2) {
            this.setState({stepIndex: stepIndex + 1});
        } else {
            // check if all items are filled
            const {selectedTheme, message, selectedLocation, sender, anonymous} = this.props.newMessage;
            if (selectedTheme.id && message && (sender || anonymous) && selectedLocation.lat) {
                // check if the lat/lng is valid

                // go to preview page
                this.props.history.push(URL.POST_NEW_MESSAGE_PREVIEW);
            } else {
                // show message that form is incomplete
                this.setState({openSnackbar: true, snackbarMessage: "please fill out all sections"});
                const self = this;
                setTimeout(() => {
                    self.setState({openSnackbar: false, snackbarMessage: ""});
                }, 2000)
            }
        }
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    onMapClick = ({x, y, lat, lng, event}) => {
        // check if location is valid

        this.props.dispatch({
            type: NEW_MESSAGE_SET_LOCATION,
            selectedLocation: {
                lat: lat, lng: lng
            }
        })
    };

    changeStepperIndex(step) {
        this.setState({
            stepIndex: step
        })
    }

    renderStepActions(step) {
        return (
            <div style={{margin: '1em 0'}}>
                <RaisedButton
                    label={step < 2 ? "Next" : "Preview"}
                    disableTouchRipple={true}
                    disableFocusRipple={true}
                    primary={true}
                    onClick={this.handleNext}
                    style={{marginRight: 12}}/>
                {step > 0 && (
                    <FlatButton
                        label="Back"
                        disableTouchRipple={true}
                        disableFocusRipple={true}
                        onClick={this.handlePrev}/>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        title: "Post New Message",
        myWeb3: state.web3js.web3js,
        contract: state.web3js.contract,
        newMessage: state.newMessage,
        messages: state.messages
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleNameChange: (event, newName) => {
            dispatch({type: NEW_MESSAGE_SET_SENDER, sender: newName});
        },

        handleMessageContentChange: (newContent) => {
            dispatch({type: NEW_MESSAGE_SET_CONTENT, message: newContent});
        },

        setClickedTheme: (theme) => {
            dispatch({type: NEW_MESSAGE_SET_THEME, theme: theme});
        },

        handleMessagePublicSwitchToggle: (event, value) => {
            dispatch({type: NEW_MESSAGE_SET_PUBLIC_VISIBILITY, messagePublic: value});
        },

        handleAnonymousSwitchToggle: (event, value) => {
            dispatch({type: NEW_MESSAGE_SET_ANONYMITY, anonymous: value});
        },

        fetchAllMessages: () => {
            dispatch(messageIsLoading(true));
            getMessages()
                .then(result => {
                    dispatch(initMessages(result));
                    dispatch(messageIsLoading(true));
                })
        },

        dispatch: dispatch
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostMessage);
