/*
 * created by aditya on 30/01/18
 */

import {makeRequest as fetch} from "../../helpers/apiWrapper";

export function getThemes() {
    // return (dispatch, getState) => {
    //     fetch({
    //         url: "/themes"
    //     }).then(result => {
    //         debugger;
    //     }).catch(error => {
    //         debugger;
    //     })
    // }
    return new Promise((resolve, reject) => {
        fetch({
            url: "/themes"
        }).then(result => {
            resolve(result.data);
        }).catch(error => {
            reject(error);
        })
    })
}

export function getMessageIPFSHash(content) {
    return fetch({
        method: "POST",
        url: "/message/getIPFSHash",
        data: {
            content
        }
    }).then(result => {
        return result.data.hash
    });
}