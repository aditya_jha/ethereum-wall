/*
 * created by aditya on 04/02/18
 */

import React from "react";
import GoogleMapReact from 'google-map-react';

export default class Map extends React.Component {
    static defaultProps = {
        center: {lat: 20.5937, lng: 78.9629},
        zoom: 1,
        mapTypeId: "roadmap",
        onChildClick: () => {}
    };

    constructor(props) {
        super();
    }

    render() {
        let {center, zoom, childComponents, onChildClick, mapTypeId, onClick} = this.props;
        if (!center.lat || !center.lng) {
            center = Map.defaultProps.center;
        }

        return (
            <div style={{height: '100%', width: '100%'}}>
                <GoogleMapReact
                    hoverDistance={60}
                    bootstrapURLKeys={{
                        key: "AIzaSyAjnvFbBmH1ONDF_CAzHHUIn0eZxJrY6Xg"
                    }}
                    options={{
                        mapTypeId: mapTypeId,
                        fullscreenControl: false,
                        styles: [
                            {
                                "featureType": "all",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "all",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#f1efe8"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape.natural",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#f1efe8"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#b2ac83"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.stroke",
                                "stylers": [
                                    {
                                        "color": "#b2ac83"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#8ac0c4"
                                    }
                                ]
                            }
                        ]
                    }}
                    onClick={onClick}
                    onChildClick={onChildClick}
                    center={center}
                    zoom={zoom}>
                    {childComponents}
                </GoogleMapReact>
            </div>
        )
    }
}