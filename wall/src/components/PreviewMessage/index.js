/*
 * created by aditya on 06/02/18
 */

import React from "react";
import DisplayMessagesMap from "./../DisplayMessagesMap";
import {connect} from "react-redux";
import * as URL from "../../data/Urls";
import RaisedButton from "material-ui/RaisedButton";
import {getMessageIPFSHash} from "../PostMessage/actions";
import {NEW_MESSAGE_SET_CONTENT_IPFS_HASH} from "../../reducers/newMessage";
import {getFormattedLatLng} from "../../helpers/utils";

class PreviewMessage extends React.Component {
    constructor(props) {
        super();
        this.state = {
            postingMessage: false
        };
    }

    componentWillMount() {
        const {selectedTheme, message, selectedLocation, sender, anonymous} = this.props.newMessage;
        if (selectedTheme.id && message && (sender || anonymous) && selectedLocation.lat) {
            // go to preview page
        } else {
            this.props.history.push(URL.POST_NEW_MESSAGE);
        }
    }

    render() {
        const {id, selectedLocation, sender, anonymous, selectedTheme, message} = this.props.newMessage;
        const {postingMessage} = this.state;

        const messages = [{
            id: id,
            lat: selectedLocation.lat,
            lng: selectedLocation.lng,
            sender: anonymous ? "anonymous" : sender,
            theme: selectedTheme,
            content: message,
            timestamp: new Date().valueOf()
        }];

        return (
            <div>
                <DisplayMessagesMap messages={messages} history={this.props.history} preview={true}/>
                <div style={{position: "absolute", bottom: 5, textAlign: "center", width: "100%"}}>
                    <RaisedButton disabled={postingMessage} onClick={this.initiatePostMessage.bind(this)}
                                  label="Post My Message" secondary={true} style={{width: 300}}/>
                </div>
            </div>
        )
    }

    initiatePostMessage = () => {
        console.log("posting message");
        this.setState({
            postingMessage: true
        });

        const {setMessageIPFSHash, WallMessagePlayContract, newMessage, myWeb3} = this.props;

        const formattedLatLng = getFormattedLatLng(newMessage.selectedLocation.lat, newMessage.selectedLocation.lng);

        // upload message to ipfs
        getMessageIPFSHash(this.props.newMessage.message)
            .then(ipfsHash => {
                setMessageIPFSHash(ipfsHash);

                // call add message function of smart contract
                myWeb3.getGasPrice()
                    .then(result => {
                        WallMessagePlayContract.addMessage(
                            formattedLatLng._lat,
                            formattedLatLng._lng,
                            formattedLatLng._latStripped,
                            formattedLatLng._lngStripped,
                            newMessage.sender,
                            newMessage.messagePublic,
                            newMessage.messageIPFSHash,
                            {
                                from: myWeb3.getAccountOwner(),
                                value: 0,
                                gas: 3000000,
                                gasPrice: result
                            },
                            (err, something) => {
                                console.log(err);
                                debugger;
                            });
                    })
                    .catch(error => {
                        console.log(error)
                    });
            })

    }
}

const mapStateToProps = (state) => {
    return {
        newMessage: state.newMessage,
        myWeb3: state.web3js.web3js,
        WallMessagePlayContract: state.web3js.contract
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setMessageIPFSHash: (messageIPFSHash) => {
            dispatch({type: NEW_MESSAGE_SET_CONTENT_IPFS_HASH, messageIPFSHash})
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PreviewMessage);