/*
 * created by aditya on 29/01/18
 */

import _404 from "./_404";
import Home from "./Home";
import PostMessage from "./PostMessage";
import AboutUs from "./AboutUs";
import BrowseMessages from "./BrowseMessages";
import MyMessages from "./MyMessages";
import Map from "./Map";
import PreviewMessage from "./PreviewMessage";
import Marker from "./Marker";

module.exports = {
    Home,
    _404,
    PostMessage,
    AboutUs,
    BrowseMessages,
    MyMessages,
    Map,
    PreviewMessage,
    Marker
};
