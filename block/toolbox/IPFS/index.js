/*
 * created by aditya on 06/02/18
 */

const ipfsAPI = require("ipfs-api");
const {host, port, options} = global.Config.ipfsConfig;

const ipfs = ipfsAPI(host, port, options);

const IPFS = {};

IPFS.ipfs = ipfs;

IPFS.addBuffer = (buffer) => {
    return new Promise((resolve, reject) => {
        ipfs.files.add(buffer, (error, result) => {
            if (error) {
                console.log(error);
                reject(error);
            } else {
                resolve(result[0]);
            }
        });
    })
};

module.exports = IPFS;

