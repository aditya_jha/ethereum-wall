/*
 * created by aditya on 12/02/18
 */

"use strict";

const {host} = global.Config.web3Config;
const Web3 = require("web3");
const fs = require("fs");
const path = require("path");

const web3 = new Web3(new Web3.providers.HttpProvider(host));

const compiledPath = path.resolve(__dirname, "../../../wall/build/contracts/WallMessagePlay.json");

const WallMessagePlayCompiled = JSON.parse(fs.readFileSync(compiledPath, "utf-8"));
let WallMessagePlay = web3.eth.contract(WallMessagePlayCompiled.abi).at("0xc1b6b9e5bdc5c801ba376a9d8b2f52d2957e5da1");

module.exports = {
    WallMessagePlay: WallMessagePlay
};