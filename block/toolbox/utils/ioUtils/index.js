/*
 * created by aditya on 30/01/18
 */

"use strict";

const fs = require("fs");
const Readable = require("stream").Readable;

function readFile(path, encoding) {
    return new Promise((resolve, reject) => {
        encoding = encoding || null;
        fs.readFile(path, encoding, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

function getStreamFromString(string) {
    let stream = new Readable();
    stream._read = function noop() {};
    stream.push(string);
    stream.push(null);

    return stream;
}

module.exports = {
    readFile,
    getStreamFromString
};
