/*
 * created by aditya on 13/02/18
 */

"use strict";

const massive = require('massive');

let connections = {};

function connect(connectionName, config) {
    return massive(config).then(instance => {
        connections[connectionName] = instance;
        return instance;
    });
}

module.exports = {
    connect: connect,
    connections: connections
};
