/*
 * created by aditya on 30/01/18
 */

"use strict";

function getPort(env) {
    return 8082;
}

function getWeb3Host(env) {
    return "http://localhost:8545";
}

function getCoreDBConfig(env) {
    return {
        host: 'localhost',
        port: 5432,
        database: 'ethereum_wall',
        user: 'aditya',
        password: '',
        ssl: false,
        poolSize: 10
    }
}

module.exports = function (env) {
    return {
        port: getPort(env),
        host: "http://127.0.0.1",
        hostAddress: `http://127.0.0.1:${getPort(env)}`,
        ipfsConfig: {
            host: "localhost",
            port: 5001,
            options: {
                protocol: "http"
            }
        },
        databases: {
            coreDB: {
                name: "CoreDB",
                config: getCoreDBConfig(env)
            }
        },
        web3Config: {
            host: getWeb3Host(env)
        }
    }
};
