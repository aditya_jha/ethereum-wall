/*
 * created by aditya on 30/01/18
 */

"use strict";

module.exports = (env) => {
    let globalConfig = require("./../globalConfig")(env);
    let config = Object.assign({}, globalConfig);

    return config;
};

