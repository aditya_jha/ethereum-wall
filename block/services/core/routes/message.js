/*
 * created by aditya on 04/02/18
 */

const router = require("express").Router();
const ipfs = require("./../../../toolbox/IPFS");

const dbConnName = global.Config.databases.coreDB.name;
const db = require("./../../../toolbox/db").connections[dbConnName];
const Messages = db.messages;

router.get("/messages", (req, res, next) => {
    Messages.find()
        .then(results => {
            res.send({
                success: !!results,
                data: results
            });
        })
});

router.post("/message", (req, res, next) => {

});

router.post("/message/getIPFSHash", (req, res, next) => {
    const buffer = new Buffer(req.body.content);
    ipfs.addBuffer(buffer)
        .then(result => {
            res.send({
                success: !!result,
                data: result
            });
        })
        .catch(next);
});

module.exports = router;