/*
 * created by aditya on 30/01/18
 */

"use strict";

module.exports = [
    require("./helloWorld"),
    require("./theme"),
    require("./message")
];
