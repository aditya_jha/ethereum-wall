/*
 * created by aditya on 30/01/18
 */

"use strict";

const router = require("express").Router();

const dbConnName = global.Config.databases.coreDB.name;
const db = require("./../../../toolbox/db").connections[dbConnName];

router.get("/themes", (req, res, next) => {
    db.themes.find()
        .then(results => {
            results.forEach(result => {
                result.icon_url = `${global.Config.hostAddress}/icons/${result.icon_name}`;
            });

            res.send({
                success: !!results,
                data: results
            });
        }).catch(next);
});

module.exports = router;