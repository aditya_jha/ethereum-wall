/*
 * created by aditya on 30/01/18
 */

"use strict";

const router = require("express").Router();

router.get("/", (req, res, next) => {
    res.send("Hello World");
});

module.exports = router;