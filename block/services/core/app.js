/*
 * created by aditya on 13/02/18
 */

"use strict";

global.Config = require("./config")(process.env.NODE_ENV || "development");
const db = require("./../../toolbox/db");

// connect to core db
const {coreDB} = global.Config.databases;
db.connect(coreDB.name, coreDB.config)
.then(instance => {
    require("./server");
})
.catch(error => {
    console.log(error);
    process.exit(1);
});
